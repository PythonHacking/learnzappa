import uuid

from django.db import models


class Content(models.Model):
    uid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=30)
    description = models.TextField()
    file_url = models.URLField()

