from django.urls import path

from blog.views.base import home

urlpatterns = [
    path(r'', home, name="home_page"),
]
