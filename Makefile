default:
	@echo "Learn Zappa main Makefile"

start-localstack:
	export $(grep -v '^#' infrastructure/local/local_infr.env | xargs) && TMPDIR=/private$TMPDIR localstack start --docker
