# Python Hacking - Learn Zappa

## Local environment
### VirtualEnv
For this project we use [PipEnv](https://pipenv.readthedocs.io/en/latest/) (for better result it is recommended to use it in tandem with [PyEnv](https://github.com/pyenv/pyenv)).

To initialize *pipenv* use command (project will use python in version 3.6.6):
```bash
pipenv --python 3.6.6
```

To activate virtual env:
```bash
pipenv shell
```

To install project requirements:
- on your computer: ```pipenv install -d```
- in docker: ```pipenv install  -d --system --ignore-pipfile```

### Infrastructure
